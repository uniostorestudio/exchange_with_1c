<?php
namespace Defo\Soap;

class SoapHelper
{
	private static $arWebServices = array(
		'SPB' => array(
			'main' => array(
				'url' => 'http://8.8.8.8:7777/spb/ws/SiteData',
				'client' => 'http://8.8.8.8:7777/spb/ws/SiteData?wsdl',
			),
			'local' => array(
				'url' => 'http://8.8.8.8/spb/ws/SiteData',
				'client' => 'http://8.8.8.8/spb/ws/SiteData?wsdl',
			)),
		'MSK' => array(
			'main' => array(
				'url' => 'http://8.8.8.8:7777/msk/ws/SiteData',
				'client' => 'http://8.8.8.8:7777/msk/ws/SiteData?wsdl',
			),
			'local' => array(
				'url' => 'http://8.8.8.8/msk/ws/SiteData',
				'client' => 'http://8.8.8.8/msk/ws/SiteData?wsdl',
			)),
		'REG' => array(
			'main' => array(
				'url' => 'http://8.8.8.8:7777/reg/ws/SiteData',
				'client' => 'http://8.8.8.8:7777/reg/ws/SiteData?wsdl',
			),
			'local' => array(
				'url' => 'http://8.8.8.8/reg/ws/SiteData',
				'client' => 'http://8.8.8.8/reg/ws/SiteData?wsdl',
			)),
		'UT' => array(
			'main' => array(
				'url' => 'http://8.8.8.8:54321/ut/ws/SiteData',
				'client' => 'http://8.8.8.8:54321/ut/ws/SiteData?wsdl',
			))
	);

	private static function createBalanceRequest($arRequest) {
		$balanceRequestSpb = new \StdClass();
		$balanceRequestSpb->TabReq = new \StdClass();
		$balanceRequestSpb->TabReq->Item = array();

		$balanceRequestMsk = new \StdClass();
		$balanceRequestMsk->TabReq = new \StdClass();
		$balanceRequestMsk->TabReq->Item = array();

		$balanceRequestReg = new \StdClass();
		$balanceRequestReg->TabReq = new \StdClass();
		$balanceRequestReg->TabReq->Item = array();

		$balanceRequestUT = new \StdClass();
		$balanceRequestUT->TabReq = new \StdClass();
		$balanceRequestUT->TabReq->Item = array();

		$arStructure = array(
			'SPB' => array(),
			'MSK' => array(),
			'REG' => array(),
			'UT' => array()
		);

		$arCitiesCode = SoapHelper::getCities();
		$arCitiesUT = $arCitiesCode["UT"];


		foreach ($arRequest as $code => $arCities) {
			// Novokuznetsk(553) from Kemerovo
			// Sayanogorsk(185) from Abakan
			unset($arCities[553]);
			unset($arCities[185]);

			foreach ($arCities as $location => $city) {
				$item = new \StdClass();

				$item->Code = $code;
				$item->City = $city;

				if ($location === 617) { // Sankt-Petersburg
					$balanceRequestSpb->TabReq->Item[] = $item;
					$arStructure['SPB'][$item->Code][] = $location;
				} else if ($location == 671) { // Moskva
					$balanceRequestMsk->TabReq->Item[] = $item;
					$arStructure['MSK'][$item->Code][] = $location;
				} else if(in_array($location, $arCitiesUT)) {
					$balanceRequestUT->TabReq->Item[] = $item;
					$arStructure['UT'][$item->Code][] = $location;
				}
				else {
					$balanceRequestReg->TabReq->Item[] = $item;
					$arStructure['REG'][$item->Code][] = $location;
				}
			}
		}

		$balanceRequest = array(
			'SPB' => $balanceRequestSpb,
			'MSK' => $balanceRequestMsk,
			'REG' => $balanceRequestReg,
			'UT' => $balanceRequestUT,
			'STRUCTURE' => $arStructure
		);

		return $balanceRequest;
	}

	public static function getCities(){
		foreach (self::$arWebServices as $codeBase => $address) {
			$client = SoapHelper::getConnect($address);
			$balanceCities = $client->BalanceСities();
			$arBalanceCities[$codeBase] = $balanceCities;
		}

		foreach($arBalanceCities as $codeBase => $arCitiesLists){
			foreach(json_decode($arCitiesLists->return) as $arCitiesList){
				if ($arCitiesList->ID){
					$arCities[$codeBase][] = (int)$arCitiesList->ID;
				}
			}
		}

		return $arCities;
	}

	private static function createBalanceRequestChanges ($site, $arCities){
		$balanceRequest = new \StdClass();
		$balanceRequest->site = $site;
		$balanceRequest->arrCities = $arCities;

		return $balanceRequest;
	}
	private static function createBalanceRequestChangesRepeatLast($site){
		$balanceRequest = new \StdClass();
		$balanceRequest->site = $site;
		$balanceRequest->Date = ConvertTimeStamp(time(), 'FULL');

		return $balanceRequest;
	}

	private static function createResponse($balanceResponse, $balanceRequest) {
		$balanceResponseSpb = $balanceResponse['SPB']->return->Item;
		$balanceResponseMsk = $balanceResponse['MSK']->return->Item;
		$balanceResponseReg = $balanceResponse['REG']->return->Item;
		$balanceResponseUT = $balanceResponse['UT']->return->Item;

		$arResponse = array();

		$i = 0;
		foreach ($balanceRequest['STRUCTURE']['SPB'] as $code => $codeCities) {
			foreach ($codeCities as $cityLocation) {
				$arResponse[$code][$cityLocation] = is_array($balanceResponseSpb)? $balanceResponseSpb[$i++]->Count: 0;
			}
		}

		$i = 0;
		foreach ($balanceRequest['STRUCTURE']['MSK'] as $code => $codeCities) {
			foreach ($codeCities as $cityLocation) {
				$arResponse[$code][$cityLocation] = is_array($balanceResponseMsk)? $balanceResponseMsk[$i++]->Count: 0;
			}
		}

		$i = 0;
		foreach ($balanceRequest['STRUCTURE']['REG'] as $code => $codeCities) {
			foreach ($codeCities as $cityLocation) {
				$arResponse[$code][$cityLocation] = is_array($balanceResponseReg)? $balanceResponseReg[$i++]->Count: 0;

				// Novokuznetsk(553) from Kemerovo
				// Sayanogorsk(185) from Abakan
				if ($cityLocation == 550) {
					$arResponse[$code][553] = $arResponse[$code][$cityLocation];
				}
				if ($cityLocation == 184) {
					$arResponse[$code][185] = $arResponse[$code][$cityLocation];
				}
			}
		}

		$i = 0;
		foreach ($balanceRequest['STRUCTURE']['UT'] as $code => $codeCities) {
			foreach ($codeCities as $cityLocation) {
				$arResponse[$code][$cityLocation] = is_array($balanceResponseUT)? $balanceResponseUT[$i++]->Count: 0;
			}
		}

		return $arResponse;
	}

	/**
	 * @var array $arRequest
	 * @throws \SoapFault
	 * @return array
	 */
	public static function getBalance($arRequest)
	{
		try {
			$balanceRequest = SoapHelper::createBalanceRequest($arRequest);

			foreach (self::$arWebServices as $codeBase => $address) {
				$client = SoapHelper::getConnect($address);

				$balanceResponse = $client->Balance($balanceRequest[$codeBase]);
				$arBalanceResponse[$codeBase] = $balanceResponse;
			}

			$arResponse = SoapHelper::createResponse($arBalanceResponse, $balanceRequest);

		} catch (\SoapFault $sf) {

			throw $sf;
		}

		return $arResponse;
	}

	public static function getBalanceChanges ($arRequest) {
		try {

			foreach (self::$arWebServices as $codeBase => $address) {

				$client = SoapHelper::getConnect($address);

				$balanceRequest = SoapHelper::createBalanceRequestChanges($arRequest, '');
				$balanceResponse = $client->BalanceChanges($balanceRequest);

				foreach (json_decode($balanceResponse->return, true) as $code => $cityList){
					foreach($cityList as $cityName => $count) {
						$arResponse[$code][$cityName] = $count;
					}
				}
			}

		} catch (\SoapFault $sf) {

			throw $sf;
		}

		return $arResponse;
	}

	public static function getBalanceRepeatLast($arRequest) {
		try {
			foreach (self::$arWebServices as $codeBase => $address) {
				$client = SoapHelper::getConnect($address);
				$balanceRequest = SoapHelper::createBalanceRequestChangesRepeatLast($arRequest);
				$balanceResponse = $client-> BalanceRepeatLast($balanceRequest);

				foreach (json_decode($balanceResponse->return, true) as $code => $cityList){
					foreach($cityList as $cityName => $count) {
						$arResponse[$code][$cityName] = $count;
					}
				}
			}

		} catch (\SoapFault $sf) {

			throw $sf;
		}

		return $arResponse;
	}

	private static function getConnect ($arWebService) {
		$options = array(
			'soap_version' => SOAP_1_2,
			'login' => 'login',
			'password' => 'password'
		);

		try {
			$options['location'] = $arWebService['main']['url'];
			$client = new \SoapClient($arWebService['main']['client'], $options);
		} catch (\SoapFault $sf) {
			if(!$arWebService['local']) {
				$arWebService['local'] = $arWebService['main'];
			}
			$options['location'] = $arWebService['local']['url'];
			$client = new \SoapClient($arWebService['local']['client'], $options);
		} catch (\SoapFault $sf) {

			throw $sf;
		}

		return $client;
	}
}
