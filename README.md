**Обмен данными с 1С через веб-сервис**

В проекте настроена мультирегиональность.
Со стороны 1С есть несколько баз: Старого образца и новый (в данный момент все филиалы поэтапно переходят на единую базу 1С:УТ)

Синхронизация происходит через веб-сервис. Обращаемся к методу 1С, на что возращаются JSON данные по списку городов с артиклем ТП, и его количеством на складах.

После чего, JSON данные обрабатываются и распределяются по складам в ТП и дублируются в большую таблицу hilghload-block с ТП. 

Связь через higload-block исторически сложилась. На момент написания кода, были не готовы отказаться от нее. Находились в стадии перехода.

**Методы обновления**

Для запуска обменна данных достаточно указать функцию:

*$BalanceAgent = new \Defo\Agents\BalanceAgent()

**Использование регулярного обновления**
Чтобы добавить действие обновления по крону, достаточно сделать:

1. Создать файл на сервере, к которому будет обращаться крон. Например /var/cron_tasks/banaceChangesAgent.php
2. Укажите подключение пролога битрикса.
3. Создаем экземпляр класса $BalanceAgent = new \Defo\Agents\BalanceAgent(); и не забываем навесить обработчики. Пример кода ниже.
4. Настройте регламмент крона.



        <?php
        $_SERVER["DOCUMENT_ROOT"] = "/var/www/..."; //путь до директории сайта
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php");
        include_once($_SERVER['DOCUMENT_ROOT'].'/local/vendor/autoload.php');

        $logfile = $_SERVER["DOCUMENT_ROOT"]."/log/balance_changes_cron_task.log"; //путь до лог файла

        $fp = fopen($logfile, 'a');
        $time = date("d.m.Y H:i:s");
        fwrite($fp, $time." 1. updateBalance start\n");


        try{
    	        $BalanceAgent = new \Defo\Agents\BalanceAgent();
        } catch (BadMethodCallException $e){
                fwrite("Ошибочка: {$e->getMessage()}\n");
                die("Ошибочка: {$e->getMessage()}\n");
        }


        fwrite($fp, $time." 1.1. ".var_export($BalanceAgent, true)." \n");

        if(!empty($BalanceAgent)){
                fwrite($fp, $time." 2. Class is found\n");
                fwrite($fp, $time." 2.1. ".var_dump($BalanceAgent)."\n");
                fwrite($fp, $time." 2.2. ".$BalanceAgent."\n");
        }

        $resUpdateBalance = $BalanceAgent::updateBalanceRepeatLast('defo');

        $resUpdateBalance = $BalanceAgent::updateBalanceChanges('defo');
        if(!empty($resUpdateBalance)){
                fwrite($fp, $time." 3. function is called\n");
        }

        fclose($fp);
